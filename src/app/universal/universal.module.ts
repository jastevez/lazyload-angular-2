import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router/';
import { CommonModule } from '@angular/common';

import { ContentComponent } from './content/content.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: ContentComponent
      }
    ])
  ],
  declarations: [
    ContentComponent
  ]
})
export class UniversalModule { }
